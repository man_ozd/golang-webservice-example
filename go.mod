module app

go 1.18

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/insanrizky/golang-webservice-example v0.0.0-20170203084105-7f571433e837 // indirect
	golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa // indirect
)
